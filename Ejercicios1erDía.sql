--Nota* escribir HR. antes de employees ejemplo: FROM hr.employees;

SELECT department_id || department_name
FROM departments;  

1.3 
DIAPOSITIVA 19
SELECT last_name, salary,
12*salary +100
FROM employees;

SELECT last_name, salary,
12*(salary +100)
FROM employees;

2.1
diapositiva 14 
SELECT salary of ' || salary || ' last_name || ' has a monthly
dollars.' AS Pay
FROM employees;

16
SELECT last_name ||' has a '|| 1 ||' year salary of '|| salary*12 ||
' dollars.' AS Pay
FROM employees;

18
SELECT department_id
FROM employees;

19
SELECT DISTINCT department_id //muestra todos los valores que existen No repetidos
FROM employees;

2.2
DIAPOSITIVA 7
SELECT employee_id, first_name, last_name
FROM employees
WHERE employee_id = 101;

9
SELECT employee_id, last_name, department_id
FROM employees
WHERE department_id = 90;

14
WHERE hire_date < '01-Jan-2000' //DEBES CONFIGURAR EL FORMATO CORRECTO DE LA FECHA EJE: 06/07/2020

2.3
DIAPOSITIVA 8
SELECT last_name, salary
FROM employees
WHERE salary BETWEEN 9000 AND 11000;

10
SELECT city, state_province, country_id
FROM locations
WHERE country_id IN('UK', 'CA');

13
SELECT last_name
FROM employees
WHERE last_name LIKE '_o%';

20
SELECT last_name, manager_id
FROM employees
WHERE manager_id IS NULL;

SELECT last_name, commission_pct
FROM employees
WHERE commission_pct IS NOT NULL;

3.1
DIAPOSITIVA 9
SELECT last_name, department_id, salary
FROM employees
WHERE department_id > 50 AND salary > 12000;

10
SELECT last_name, hire_date, job_id
FROM employees
WHERE hire_date > '01-Jan-1998' AND job_id LIKE 'SA%';

13
SELECT last_name||' '||salary*1.05 As "Employee Raise"
FROM employees
WHERE department_id IN(50,80) AND first_name LIKE 'C%'
OR last_name LIKE '%s%';

16
SELECT last_name||' '||salary*1.05 As "Employee Raise", department_id,
first_name
FROM employees
WHERE department_id IN(50,80)
OR first_name LIKE 'C%'
AND last_name LIKE '%s%';

3.2
DIAPOSITIVA 8
SELECT last_name, hire_date
FROM employees
ORDER BY hire_date;

9
SELECT last_name, hire_date
FROM employees
ORDER BY hire_date DESC;

10
SELECT last_name, hire_date AS "Date Started"
FROM employees
ORDER BY "Date Started" DESC;

14
SELECT department_id, last_name
FROM employees
WHERE department_id <= 50
ORDER BY department_id, last_name;

15
SELECT department_id, last_name
FROM employees
WHERE department_id <= 50
ORDER BY department_id DESC, last_name;

FUNCIONES 4.1 DIAPOSITIVA 11 --FUNCION LAWER CONVIERTE A MINUSCULAS.
SELECT last_name
FROM employees
WHERE LOWER(last_name) = 'abel';

UPPER-- PASA A MAYUSCULAS

12
SELECT last_name
FROM employees
WHERE INITCAP(last_name) = 'Abel';

14
SELECT CONCAT('Hello', 'World')
FROM DUAL;

17
SELECT last_name, INSTR(last_name, 'a')
FROM employees;

18
SELECT LPAD(last_name, 10, '*')
FROM employees;

25
SELECT LOWER(last_name)|| LOWER(SUBSTR(first_name,1,1))
AS "User Name"
FROM employees;

28
SELECT first_name, last_name, salary, department_id
FROM employees
WHERE department_id=:enter_dept_id;

30
SELECT *
FROM employees
WHERE last_name = :l_name;

4.2 DIAPOSITIVA 8
ROUND(45.926, 2) //REDONDEA

9
TRUNC (45.926, 2) 45.92

12 MOOD--RECIDUO
SELECT country_name, MOD(airports,2)
AS "Mod Demo"
FROM wf_countries;

SELECT MOD(46,2)
FROM dual

4.3
DIAPOSITIVA 6
SELECT SYSDATE
FROM dual;SELECT last_name, hire_date + 60
FROM employees;
