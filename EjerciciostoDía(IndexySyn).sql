/********* INDEX************/
--Sirven para mejorar el tiempo de consulta de ciertos registros, ya que as� obtenemos la direcci�n de memoria del registro, 
CREATE INDEX wf_cont_reg_id_idx
ON countries(region_id);

CREATE INDEX emps_name_idx
ON employees(first_name, last_name);

SELECT DISTINCT ic.index_name, ic.column_name,
ic.column_position, id.uniqueness
FROM user_indexes id, user_ind_columns ic
WHERE id.table_name = ic.table_name
AND ic.table_name = 'EMPLOYEES';


CREATE INDEX upper_last_name_idx
ON employees (UPPER(last_name));

SELECT *
FROM employees
WHERE UPPER(last_name) = 'KING';

CREATE INDEX emp_hire_year_idx
ON employees (TO_CHAR(hire_date, 'yyyy'));

SELECT first_name, last_name, hire_date
FROM employees
WHERE TO_CHAR(hire_date, 'yyyy') = '2004';


DROP INDEX emp_hire_year_idx;

/**************SYNONYMS************/

CREATE SYNONYM copy_em
FOR copy_employees;