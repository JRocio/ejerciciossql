
/*Creamos objetos tipo PERSON y ADDRESS, as� que ahora al crear una tabla, podemos poner campos de ese tipo*/

CREATE TABLE persons OF PERSON

CREATE TABLE  employees
( 
  empnumber            INTEGER PRIMARY KEY,
  person_data     REF  PERSON,  --Quiere decir que lo manejamos atravez de una referencia, que es la tabla persons
  manager         REF  PERSON,
  office_addr          ADDRESS, --Aqui lo manejamos directamente, sin una tabla de referencia
  salary               NUMBER
)

/*** Insert some data--2 objects into the persons typed table ***/
INSERT INTO persons VALUES (
            PERSON('Wolfgang Amadeus Mozart', 123456,
               ADDRESS('Am Berg 100', 'Salzburg', 'AT','10424')))
/
INSERT INTO persons VALUES (
            PERSON('Ludwig van Beethoven', 234567,
               ADDRESS('Rheinallee', 'Bonn', 'DE', '69234')))
/

INSERT INTO employees (empnumber, office_addr, salary) 
VALUES (1001,
        ADDRESS('500 Oracle Parkway', 'Redwood Shores', 'CA', '94065'),
        50000)
        
 /       
UPDATE employees 
   SET manager =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Wolfgang Amadeus Mozart')
/
UPDATE employees 
   SET person_data =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven')