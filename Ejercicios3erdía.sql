TERCER DIA

SELECT hire_date, employee_id, job_id --Tenemos tres datos
FROM employees
UNION
SELECT TO_DATE(NULL),employee_id, job_id--Tambi�n debemos tener tres datos, y deben ser del mismo tipo que en el SELECT anterior, para que pueda haber union
FROM job_history
ORDER BY employee_id;

/*De igual forma estamos empatando las estructuras (Tipos de datos), para poder unir las tablas*/
SELECT hire_date, employee_id, TO_DATE(null) start_date1,TO_DATE(null) end_date1, job_id, department_id
FROM employees
UNION
SELECT TO_DATE(null), employee_id, start_date, end_date, job_id, department_id
FROM job_history
ORDER BY employee_id;


--SUBQUERY

SELECT first_name, last_name,hire_date
FROM employees
WHERE hire_date > (SELECT hire_date FROM employees WHERE last_name = 'Vargas');  
--Puede haber un problema si regresa mas de un resultado "Vargas"

SELECT round(AVG(salary), 2)--Redondeo del promedio del salario
FROM employees
WHERE department_id = 90; 
SELECT last_name
FROM employees
WHERE salary = (SELECT MAX(salary)FROM employees);

SELECT last_name
FROM employees
WHERE department_id =(SELECT department_id FROM employees WHERE last_name = 'Grant');

-- Query interior es de la tabla departamentos y el exterior es de la tabla empleados
SELECT last_name, job_id, department_id
FROM employees
WHERE department_id =(SELECT department_id FROM departments WHERE department_name = 'Marketing')
ORDER BY job_id;

SELECT last_name, job_id, salary, department_id
FROM employees
WHERE job_id =(SELECT job_id FROM employees WHERE employee_id = 141) 
AND department_id =(SELECT department_id FROM departments WHERE location_id = 1500);
-- Obtenemos el job Id del empleado 141 y en departamento con id cuya locaci�n es 1500

-- SUBQUERY SINGLE ROW
SELECT last_name, salary
FROM employees
WHERE salary <(SELECT AVG(salary) FROM employees);

SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) >(SELECT MIN(salary) FROM employees WHERE department_id = 50)
ORDER BY department_id;

--SUBQUERY MULTIPLE-ROW
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN (SELECT EXTRACT(YEAR FROM hire_date) FROM employees WHERE department_id=90);
--Para evitar el error usamos IN 

SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) < ANY (SELECT EXTRACT(YEAR FROM hire_date) FROM employees WHERE department_id=90);
/* El ANY hace que lo tome con que cumpla una sola de las condiciones que se genran cuando consulta todos aquellos registros 
del department_id=90, en este caso como regresa tres a�os 2001,2003 y 2005, entonces al tener un menor que < entra cualquiera que fuese
contratado antes del 2005*/


SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) > ALL (SELECT EXTRACT(YEAR FROM hire_date) FROM employees WHERE department_id=90);
/*En este caso debe cumplir todas las condiciones,es decir menor o mayor que los tres a�os*/

--Group by and Having
SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) < ANY(SELECT salary FROM employees WHERE department_id IN (10,20))
ORDER BY department_id;


SELECT employee_id, manager_id, department_id
FROM employees
WHERE(manager_id,department_id) IN(SELECT manager_id,department_id FROM employees WHERE employee_id IN (149,174))
AND employee_id NOT IN (149,174);



INSERT INTO copy_employees
(employee_id, first_name, last_name, email, phone_number, hire_date,
job_id, salary)
VALUES
(303,'Angelina','Wright', 'awright','4159982010',
TO_DATE('Julio 10, 2017 17:20', 'Month fmdd, yyyy HH24:MI'),
'MK_REP', 3600); 

SELECT first_name, last_name, TO_CHAR(hire_date, 'dd-Mon-YYYY HH24:MI') As "Date and Time"
FROM copy_employees
WHERE employee_id = 303;