/*MERGE, Va a insertar un registro, busca y si encuentra, solo hace update, si no, hace un nuevo registro*/

MERGE INTO copy_employees c USING employees e
ON (c.employee_id = e.employee_id)
WHEN MATCHED THEN UPDATE
    SET
    c.last_name = e.last_name,
    c.department_id = e.department_id
WHEN NOT MATCHED THEN 
INSERT (employee_id,last_name,department_id)
VALUES (e.employee_id, e.last_name, e.department_id);

/*INSERT ALL-  Cuando quiero insertar en dos tablas o mas, los registros de otra tabla*/
INSERT ALL
    INTO my_employees
    VALUES (hire_date, first_name, last_name)
    
    INTO copy_my_employees
    VALUES (hire_date, first_name, last_name)
    
SELECT hire_date, first_name, last_name
FROM employees;


INSERT ALL
WHEN call_ format IN ('tlk','txt','pic') THEN
    INTO all_calls
    VALUES (caller_id, call_timestamp, call_duration, call_format)
WHEN call_ format IN ('tlk','txt') THEN
    INTO police_record_calls
    VALUES (caller_id, call_timestamp, recipient_caller)
WHEN call_duration < 50 AND call_type = 'tlk' THEN
    INTO short_calls
    VALUES (caller_id, call_timestamp, call_duration)
WHEN call_duration > = 50 AND call_type = 'tlk' THEN
    INTO long_calls
    VALUES (caller_id, call_timestamp, call_duration)
SELECT caller_id, call_timestamp, call_duration, call_format,
recipient_caller --Todos los campos que voy a necesitar para insertar en las diversas tablas 
FROM calls
WHERE TRUNC(call_timestamp ) = TRUNC(SYSDATE);

/*CREATE TABLES*/
CREATE TABLE my_friends
(first_name VARCHAR2(20),
last_name VARCHAR2(30),
email VARCHAR2(30),
phone_num VARCHAR2(12),
birth_date DATE);


/*Consulta de las tablas de USUARIO*/

SELECT table_name, status
FROM USER_TABLES;

/*Comentarios de una tablla*/
SELECT table_name, comments
FROM user_tab_comments;

--Hacemos un cambio en la tabla, para posteriormente ver que cambios de hicieron
INSERT INTO copy_employees
VALUES (1, 'Natacha', 'Hansen', 'NHANSEN', '4412312341234','07-SEP-1998', 'AD_VP', 12000, null, 100, 90);

--AQUI REALIZAMOS LA CONSULTA DEL CAMBIO QUE SE REALIZÓ
SELECT employee_id,first_name ||' '|| last_name AS "NAME",versions_operation AS "OPERATION",versions_starttime AS "START_DATE",versions_endtime AS "END_DATE", salary
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;


/*VISTAS-- Podemos usarlas para guardar el resultado de una consulta de un query, simplifficando cuando queramos
volver a revisar el resultado de esa consulta, mandando llamar solo de la vista*/
CREATE VIEW view_employees
AS SELECT employee_id,first_name, last_name, email
FROM employees
WHERE employee_id BETWEEN 100 and 124;

SELECT *
FROM view_employees;

------------------------------
CREATE VIEW view_dept50
AS SELECT department_id, employee_id, first_name, last_name, salary
FROM copy_employees
WHERE department_id = 50;

SELECT count(*) FROM view_dept50;

UPDATE view_dept50
SET department_id = 90
WHERE employee_id = 124;

CREATE OR REPLACE VIEW view_dept50
AS SELECT department_id, employee_id, first_name, last_name, salary
FROM employees
WHERE department_id = 50
WITH READ ONLY; /*Esta linea de comando nos permite que no se pueda modificar la base de datos desde la Vista*/

--Inline Views

SELECT e.last_name, e.salary, e.department_id, d.maxsal
FROM employees e,
    (SELECT department_id, max(salary) maxsal FROM employees GROUP BY department_id) d
WHERE e.department_id = d.department_id
AND e.salary = d.maxsal;

/**************ROWNUM ayuda en la páginación*/
SELECT ROWNUM AS "Posicion", last_name, hire_date 
FROM employees
WHERE ROWNUM <=2 /*Toma  los primeros dos registros y los muestra*/
ORDER BY hire_date; --Ordena esos 2 registros por su fecha de contratación

SELECT ROWNUM AS "Longest employed", last_name, hire_date
FROM (SELECT last_name, hire_date FROM employees ORDER BY hire_date)/*Primero ordena todos los empleados por fecha de contratación*/
WHERE ROWNUM <=5;--Una vez que estan ordenados por fecha de contratación toma los 5 mas altos y así obtenemos los 5 mas antiguos.

/*********SEQUENCE**********/
CREATE SEQUENCE runner_id_seq
INCREMENT BY 1
START WITH 1
MAXVALUE 50000
NOCACHE
NOCYCLE;

SELECT sequence_name, min_value, max_value, increment_by, last_number
FROM user_sequences;

INSERT INTO departments
(department_id, department_name, location_id)
VALUES (departments_seq.NEXTVAL, 'Support', 2500);  
/*Insertamos un valor en la tabla con el siguiente valor de la secuencia */